use futures::prelude::*;

#[tokio::main]
async fn main() {
    let mut watcher = taskwarrior::tokio::TokioSource::new().await;
    let mut watcher = std::pin::Pin::new(&mut watcher);
    while let Some(change) = watcher.next().await {
        let tasks = watcher.freeze();
        for task in tasks.into_iter().take(3) {
            println!("{:?}", task);
        }
        println!("{:?} ({} pending tasks)", change, tasks.pending_len());
    }
}
