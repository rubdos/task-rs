use futures::prelude::*;

#[tokio::main]
async fn main() {
    let mut watcher = taskwarrior::tokio::TokioSource::new().await;
    let mut watcher = std::pin::Pin::new(&mut watcher);
    if let Some(change) = watcher.next().await {
        let tasks = watcher.freeze();
        for task in tasks.into_iter().take(10) {
            println!("{:?}", task);
        }
        println!(
            "{:?} ({} tasks, {} pending)",
            change,
            tasks.total_len(),
            tasks.pending_len()
        );
        println!("Projects: {:?}", tasks.projects());
    }
}
