use std::process::Command;

use taskwarrior::model::ParsedTask;

fn main() {
    let output = Command::new("task")
        .arg("export")
        .output()
        .expect("Failed to execute task");
    let tasks: Vec<ParsedTask> = serde_json::from_slice(&output.stdout).unwrap();

    for task in tasks {
        for (key, val) in task.unknown {
            if key.starts_with("gitlab") || key.starts_with("github") {
                continue;
            }
            println!("Key {} (val {})", key, val);
        }
    }
}
