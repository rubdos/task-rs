use futures::prelude::*;
use inotify::{EventStream, Inotify, WatchMask};
use std::{pin::Pin, task::Poll, time::Duration};
use tokio::time::Instant;

use crate::model::*;
use crate::set::{ChangeLog, TaskSet};

type UpdateTask = dyn Future<Output = anyhow::Result<Vec<u8>>>;

pub struct TokioSource {
    current_set: TaskSet,
    /// A refresh timer, because urgency needs an update now and then.
    refresh: tokio::time::Interval,

    update_task: Option<Pin<Box<UpdateTask>>>,

    notify: Option<Pin<Box<EventStream<Vec<u8>>>>>,
}

async fn update() -> anyhow::Result<Vec<u8>> {
    use tokio::process::*;
    let output = Command::new("task").arg("export").output().await?;
    if !output.status.success() {
        anyhow::bail!("`task export` returned with an error; please file a bug.");
    }
    Ok(output.stdout)
}

impl TokioSource {
    fn inotify() -> anyhow::Result<EventStream<Vec<u8>>> {
        let mut notify = Inotify::init().expect("inotify initialization");
        let taskdir = std::path::PathBuf::from(std::env::var("HOME")?);
        notify.add_watch(
            taskdir.join(".task"),
            WatchMask::MODIFY | WatchMask::CLOSE_WRITE | WatchMask::DELETE,
        )?;

        let notify = notify.event_stream(vec![0; 1024])?;

        Ok(notify)
    }

    pub async fn new() -> Self {
        Self::new_with_refresh(Duration::from_secs(30)).await
    }

    pub async fn new_with_refresh(refresh: Duration) -> Self {
        TokioSource {
            current_set: TaskSet::default(),
            refresh: tokio::time::interval(refresh),
            update_task: None,
            notify: Self::inotify()
                .map_err(|e| {
                    eprintln!("error in inotify: {}", e);
                })
                .ok()
                .map(Box::pin),
        }
    }

    pub fn freeze(&self) -> &TaskSet {
        &self.current_set
    }

    fn maybe_force_refresh(&mut self) {
        if self.update_task.is_none() {
            self.force_refresh();
        }
    }

    pub fn force_refresh(&mut self) {
        self.update_task = Some(Box::pin(update()));
    }

    fn postpone_refresh(&mut self) {
        let duration = self.refresh.period();
        self.refresh = tokio::time::interval_at(Instant::now() + duration, duration);
    }
}

impl Stream for TokioSource {
    type Item = ChangeLog;

    fn poll_next(
        mut self: Pin<&mut Self>,
        cx: &mut std::task::Context,
    ) -> Poll<Option<Self::Item>> {
        while let Some(not) = self.notify.as_mut() {
            if let Poll::Ready(ev) = not.as_mut().poll_next(cx) {
                match ev {
                    Some(_ev) => {
                        self.maybe_force_refresh();
                        self.postpone_refresh();
                    }
                    None => {
                        eprintln!("inotify ended");
                        self.notify = None;
                        break;
                    }
                }
            } else {
                break;
            }
        }

        match self.refresh.poll_tick(cx) {
            Poll::Ready(_ready) => self.maybe_force_refresh(),
            Poll::Pending => (),
        }

        if let Some(task) = self.update_task.as_mut() {
            let task = Pin::new(task);
            let json = match futures::ready!(task.poll(cx)) {
                Ok(json) => json,
                Err(e) => {
                    log::error!("error updating task data: {}", e);
                    return Poll::Pending;
                }
            };
            self.update_task = None;
            let tasks: Vec<ParsedTask> =
                serde_json::from_slice(&json).expect("deserializable JSON");

            match self.current_set.update_from_complete(tasks) {
                Some(change) => Poll::Ready(Some(change)),
                None => Poll::Pending,
            }
        } else {
            Poll::Pending
        }
    }
}

#[cfg(test)]
mod tests {
    #[tokio::test]
    async fn foo() {}
}
