use std::borrow::Cow;
use std::collections::HashMap;

use chrono::{DateTime, Duration, Utc};
use chrono::{NaiveDateTime, TimeZone};
use uuid::Uuid;

const DATETIME_FORMAT: &str = "%Y%m%dT%H%M%SZ";

fn parse_datetime_str(s: &str) -> Result<DateTime<Utc>, chrono::ParseError> {
    if s.contains(|x: char| !x.is_numeric()) {
        NaiveDateTime::parse_from_str(s, DATETIME_FORMAT)
            .map_err(|x| {
                println!("{}", x);
                x
            })
            .map(|x| x.and_utc())
    } else {
        Ok(Utc.timestamp_opt(s.parse().unwrap(), 0).unwrap())
    }
}

mod dt_format_optional {
    use chrono::{DateTime, NaiveDateTime, TimeZone, Utc};
    use serde::{self, Deserialize, Deserializer, Serializer};

    pub fn serialize<S>(date: &Option<DateTime<Utc>>, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match date {
            Some(date) => {
                serializer.serialize_some(&format!("{}", date.format(super::DATETIME_FORMAT)))
            }
            None => serializer.serialize_none(),
        }
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<Option<DateTime<Utc>>, D::Error>
    where
        D: Deserializer<'de>,
    {
        match Option::<&'de str>::deserialize(deserializer)? {
            Some(s) => super::parse_datetime_str(s)
                .map_err(serde::de::Error::custom)
                .map(Some),
            None => Ok(None),
        }
    }
}

mod duration_optional {
    use chrono::Duration;
    use serde::{self, Deserialize, Deserializer, Serializer};

    pub fn serialize<S>(date: &Option<Duration>, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match date {
            Some(_date) => {
                unimplemented!()
            }
            None => serializer.serialize_none(),
        }
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<Option<Duration>, D::Error>
    where
        D: Deserializer<'de>,
    {
        match Option::<&'de str>::deserialize(deserializer)? {
            Some(s) => {
                let regex =
                    regex::Regex::new(r"^P(nY)?(\d+M)?(\d+D)?(T(\d+H)?(\d+M)?(\d+S)?)?$").unwrap();
                let captures = regex
                    .captures(s)
                    .ok_or_else(|| serde::de::Error::custom(format!("Unparsable duration: {s}")))?;
                let parse_capture = |idx: usize| -> Result<i64, D::Error> {
                    captures
                        .get(idx)
                        .map(|x| {
                            let x = x.as_str();
                            // Drop the suffix
                            x[..x.len() - 1].parse().map_err(serde::de::Error::custom)
                        })
                        .transpose()
                        .map(Option::unwrap_or_default)
                };
                let years = parse_capture(1)?;
                let months = parse_capture(2)?;
                let days = parse_capture(3)?;
                let hours = parse_capture(5)?;
                let minutes = parse_capture(6)?;
                let seconds = parse_capture(7)?;

                // XXX Duration does not retain all information that's encoded.
                let dur = Duration::days(years * 365 + months * 30 + days)
                    + Duration::hours(hours)
                    + Duration::minutes(minutes)
                    + Duration::seconds(seconds);

                Ok(Some(dur))
            }
            None => Ok(None),
        }
    }
}

mod dt_format {
    use chrono::{DateTime, NaiveDateTime, TimeZone, Utc};
    use serde::{self, Deserialize, Deserializer, Serializer};

    pub fn serialize<S>(date: &DateTime<Utc>, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let s = format!("{}", date.format(super::DATETIME_FORMAT));
        serializer.serialize_str(&s)
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<DateTime<Utc>, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = <&'de str>::deserialize(deserializer)?;
        super::parse_datetime_str(s).map_err(serde::de::Error::custom)
    }
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub enum Priority {
    #[serde(rename(serialize = "L", deserialize = "L"))]
    Low,
    #[serde(rename(serialize = "M", deserialize = "M"))]
    Medium,
    #[serde(rename(serialize = "H", deserialize = "H"))]
    High,
}

#[derive(Debug, serde::Deserialize, serde::Serialize, Eq, PartialEq)]
#[serde(rename_all = "lowercase")]
pub enum Status {
    Waiting,
    Pending,
    Recurring,
    Completed,
    Deleted,
}

#[derive(Debug, serde::Deserialize)]
pub struct Annotation<'a> {
    #[serde(borrow)]
    pub description: Cow<'a, str>,
    #[serde(with = "dt_format")]
    pub entry: DateTime<Utc>,
}

#[derive(Debug, serde::Deserialize)]
pub struct ParsedTask<'a> {
    pub id: usize,
    pub uuid: Uuid,
    #[serde(borrow)]
    pub description: Cow<'a, str>,
    #[serde(with = "dt_format")]
    pub entry: DateTime<Utc>,
    #[serde(with = "dt_format")]
    pub modified: DateTime<Utc>,
    pub priority: Option<Priority>,
    #[serde(with = "dt_format_optional", default)]
    pub reviewed: Option<DateTime<Utc>>,
    #[serde(with = "dt_format_optional", default)]
    pub end: Option<DateTime<Utc>>,
    #[serde(with = "dt_format_optional", default)]
    pub due: Option<DateTime<Utc>>,
    #[serde(with = "dt_format_optional", default)]
    pub scheduled: Option<DateTime<Utc>>,

    #[serde(with = "duration_optional", default)]
    // UDA "duration"
    pub duration: Option<Duration>,

    pub status: Status,
    pub urgency: f32,

    #[serde(default)]
    pub annotations: Vec<Annotation<'a>>,

    #[serde(default)]
    pub tags: Vec<Cow<'a, str>>,
    pub project: Option<Cow<'a, str>>,

    pub parent: Option<Uuid>,
    // TODO: maybe make this an enum
    pub recur: Option<Cow<'a, str>>,

    #[serde(flatten)]
    pub unknown: HashMap<&'a str, serde_json::Value>,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse() {
        let tasks = r#"[
{"id":1,"description":"Fixup harbour-bugwarrior-GUI","entry":"20200608T103107Z","modified":"20210303T124044Z","priority":"L","reviewed":"20210303T124044Z","status":"pending","tags":["personal"],"uuid":"b5a607e1-6646-4ee3-8dec-c86c57892a24","urgency":4.6},
{"id":2,"description":"Belastingsaangifte indienen","due":"20200630T215944Z","entry":"20200608T111228Z","mask":"++-","modified":"20210920T151341Z","recur":"yearly","reviewed":"20200609T115230Z","status":"recurring","uuid":"d4904df8-57c1-4853-bd70-61aa0a583ca7","urgency":14},
{"id":163,"description":"Draw something","due":"20230507T215959Z","duration":"PT30M","entry":"20230503T145049Z","modified":"20230504T121613Z","scheduled":"20230504T160000Z","status":"pending","uuid":"fc1ca6d4-b82b-4569-a7cb-8b3bc5fef4cc","urgency":7.24343},
{"id":45,"S2020_regression":"x","bug":"x","description":"(bw)Is#336 - Already found a session for <(not my own) UUID>. Refusi","entry":"20210422T101357Z","gitlabassignee":"rubdos","gitlabauthor":"thmichel","gitlabcreatedon":"20210422T101357Z","gitlabdescription":"Not sure if it actually impacts functionality, found this during troubleshooting sending messages. Error comes up on startup with multiple times with different UIDs:\n\nERROR harbour_whisperfish::worker::client::migrations::e164_to_uuid] Already found a session for <UID>. Refusing to overwrite. Please file a bug report\n\nMight be related to https:\/\/gitlab.com\/whisperfish\/whisperfish\/-\/issues\/326","gitlabdownvotes":0.000000,"gitlabmilestone":"v0.6.0: first release after rewrite","gitlabnamespace":"whisperfish","gitlabnumber":"336","gitlabrepo":"whisperfish","gitlabstate":"opened","gitlabtitle":"Already found a session for <(not my own) UUID>. Refusing to overwrite","gitlabtype":"issue","gitlabupdatedat":"20220903T215105Z","gitlabupvotes":3.000000,"gitlaburl":"https:\/\/gitlab.com\/whisperfish\/whisperfish\/issues\/336","gitlabwip":0.000000,"modified":"20220904T185058Z","priority":"M","priority__2":"x","project":"whisperfish","severity__2_medium":"x","status":"pending","uuid":"dd03736c-5e20-48bd-836b-88378109af88","annotations":[{"entry":"20210430T140452Z","description":"https:\/\/gitlab.com\/whisperfish\/whisperfish\/issues\/336"}],"tags":["git:S2020_regression","git:bug","git:priority__2","git:severity__2_medium"],"urgency":8.7},
{"id":0,"description":"Belastingsaangifte indienen","due":"20230630T215944Z","end":"20230902T142039Z","entry":"20220701T155757Z","imask":3.000000,"modified":"20230902T142040Z","parent":"d4904df8-57c1-4853-bd70-61aa0a583ca7","recur":"yearly","reviewed":"1591703550","rtype":"periodic","status":"completed","uuid":"e5c6e19d-3376-4a8c-a524-e4dc825ae57a","urgency":14}
]"#;

        let _tasks: Vec<ParsedTask> = serde_json::from_str(tasks).unwrap();
    }
}
