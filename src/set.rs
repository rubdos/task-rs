use chrono::{DateTime, Duration, Utc};
use std::{cmp::Ordering, collections::HashMap, ops::Index};
use uuid::Uuid;

use crate::model::{ParsedTask, Status};

#[derive(Debug, Clone)]
pub struct Task {
    pub id: usize,
    pub uuid: Uuid,
    pub description: String,
    pub project: Option<String>,
    pub urgency: f32,
    pub due: Option<DateTime<Utc>>,
    pub scheduled: Option<DateTime<Utc>>,
    // UDA
    pub duration: Option<Duration>,
}

#[derive(Debug, Clone)]
pub struct Project {
    pub name: String,
    pub tasks: usize,
}

#[derive(Default, Debug, Clone)]
pub struct TaskSet {
    tasks: HashMap<Uuid, Task>,
    /// The active tasks ordered by urgency
    ordered: Vec<Uuid>,

    /// List of projects ordered by amount of tasks
    projects: Vec<Project>,
}

impl Task {
    fn from_parsed(source: ParsedTask) -> Task {
        Task {
            id: source.id,
            uuid: source.uuid,
            description: source.description.into_owned(),
            project: source.project.map(std::borrow::Cow::into_owned),
            urgency: source.urgency,
            due: source.due,
            scheduled: source.scheduled,
            duration: source.duration,
        }
    }
}

/// Retrieve ordered by urgency
impl Index<usize> for TaskSet {
    type Output = Task;

    fn index(&self, i: usize) -> &Task {
        let uuid = self.ordered[i];
        &self[uuid]
    }
}

impl<'a> Index<&'a Uuid> for TaskSet {
    type Output = Task;

    fn index(&self, uuid: &'a Uuid) -> &Task {
        &self.tasks[uuid]
    }
}

impl Index<Uuid> for TaskSet {
    type Output = Task;

    fn index(&self, uuid: Uuid) -> &Task {
        &self.tasks[&uuid]
    }
}

impl<'a> IntoIterator for &'a TaskSet {
    type Item = &'a Task;

    type IntoIter = Box<dyn Iterator<Item = Self::Item> + 'a>;

    fn into_iter(self) -> Self::IntoIter {
        Box::new((0..self.pending_len()).map(move |i| &self[i]))
    }
}

impl TaskSet {
    pub fn total_len(&self) -> usize {
        self.tasks.len()
    }

    pub fn pending_len(&self) -> usize {
        self.ordered.len()
    }

    pub fn is_empty(&self) -> bool {
        self.total_len() == 0
    }

    pub fn update_from_complete<'a>(
        &mut self,
        tasks: impl IntoIterator<Item = ParsedTask<'a>>,
    ) -> Option<ChangeLog> {
        let old_tasks = std::mem::take(&mut self.tasks);
        let _old_order = std::mem::take(&mut self.ordered);

        let mut task_updated = false;
        let mut task_added = false;

        for task in tasks {
            let uuid = task.uuid; // copy

            if task.status == Status::Pending {
                // Projects maintenance
                if let Some(name) = &task.project {
                    if let Some(project) = self.projects.iter_mut().find(|p| &p.name == name) {
                        project.tasks += 1;
                    } else {
                        self.projects.push(Project {
                            name: name.to_string(),
                            tasks: 1,
                        });
                    }
                }

                let position = self.ordered.binary_search_by(|x| {
                    let pivot = &self.tasks[x];
                    pivot
                        .urgency
                        .partial_cmp(&task.urgency)
                        .unwrap_or(Ordering::Equal)
                        .reverse()
                });
                let position = match position {
                    Ok(i) => i,
                    Err(i) => i,
                };
                self.ordered.insert(position, uuid);
            }
            assert!(
                self.tasks
                    .insert(task.uuid, Task::from_parsed(task))
                    .is_none(),
                "no duplicate tasks"
            );

            match (old_tasks.get(&uuid), self.tasks.get(&uuid)) {
                (_, None) => unreachable!("inserted task not found"),
                (None, Some(_)) => task_added = true,
                (Some(old), Some(new)) => {
                    if old.description != new.description {
                        task_updated = true;
                    }
                    if old.urgency != new.urgency {
                        task_updated = true;
                    }
                }
            }
        }

        // Projects maintenance
        let number = self.tasks.len();
        self.projects.sort_unstable_by_key(|x| number - x.tasks);

        if old_tasks.is_empty() && !self.tasks.is_empty() {
            return Some(ChangeLog::Initialized);
        }
        if task_added {
            return Some(ChangeLog::TaskAdded);
        }
        if task_updated {
            return Some(ChangeLog::TaskUpdated);
        }
        None
    }

    pub fn projects(&self) -> &[Project] {
        &self.projects
    }
}

#[derive(Debug, Clone)]
pub enum ChangeLog {
    Initialized,
    TaskUpdated,
    TaskAdded,
}
